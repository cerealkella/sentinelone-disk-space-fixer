<#
    2023-03-04
    SentinelOne Volume Shadow Copy Fixer Script
    Needed to run this manually too many times on too many systems.
    For when a system has been eaten up by VSS copies and needs space
    Ensure the Agent version is correct with what is installed
    on the target system.
    This will delete all volume shadow copies, 
    so please ensure you don't need them.
#>
Write-Host "Disk Space:"
fsutil volume diskfree c:
cd "C:\Program Files\SentinelOne\Sentinel Agent 22.2.5.806\"
Write-Host "SentinelOne Mess Cleaner" -ForegroundColor Green
$passcode= Read-Host -Prompt "Enter your SentinelOne Passcode"
Write-Host "The entered city is" $passcode -ForegroundColor Green
Write-Host "Unprotecting your system to free up space..." -ForegroundColor Red
.\sentinelctl.exe unprotect -k "$passcode"
.\sentinelctl.exe config vssConfig.vssProtection false
.\sentinelctl.exe config enginesWantedState.penetration off
.\sentinelctl.exe unload -m -a
vssadmin delete shadows /all /quiet
Write-Host "Deleted VSS Shadow Copies" -ForegroundColor Green
Write-Host "Re-protecting your system!" -ForegroundColor Green
.\sentinelctl.exe config vssConfig.vssProtection true
.\sentinelctl.exe config enginesWantedState.penetration local
.\sentinelctl.exe load -m -a
.\sentinelctl.exe protect
Write-Host "Your system has been re-protected" -ForegroundColor Green
Write-Host "Disk Space:"
fsutil volume diskfree c:
